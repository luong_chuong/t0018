<?php $id="syokuin";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<section class="p-syokuin1">
	<div class="l-container">
		<!-- title -->
		<div class="c-titleSection">
			<h2>募集要項</h2>
			<p>Recruitment</p>
		</div>
		<!-- content -->
		<div class="c-table2">
			<table>
				<tr>
					<th>事業所名</th>
					<td>鹿追町農業協同組合</td>
				</tr>
				<tr>
					<th>所在地</th>
					<td>北海道河東郡鹿追町新町4丁目51番地</td>
				</tr>
				<tr>
					<th>全従業員数</th>
					<td>184名</td>
				</tr>
				<tr>
					<th>事業内容</th>
					<td>信用・共済・販売・購買・利用・指導</td>
				</tr>
				<tr>
					<th>採用職種</th>
					<td>正職員（総合職・農産技術職・酪農技術職）</td>
				</tr>
				<tr>
					<th>学歴</th>
					<td>大学卒（年齢不問）</td>
				</tr>
				<tr>
					<th>勤務時間</th>
					<td>
						<p>■交代制 ： 無</p>
						<p>【4月〜10月】　８：３０〜１７：００</p>
						<p>【11月〜3月】　９：００〜１７：００</p>
						<p>■休憩時間　６０分</p>
						<p>■変形労働時間制 ： 有</p>
						<p>■残業 ： 月平均４時間</p>
					</td>
				</tr>
				<tr>
					<th>給与</th>
					<td>
						<p>■基本給 ： 184,000円（新卒者。経験者は別途対応</p>
						<p>■住宅手当 ： 4,700円</p>

					</td>
				</tr>
				<tr>
					<th>賞与</th>
					<td>
						<p>採用１年後</p>
						<p>・　６月・・・基本給×1.05の1.85ヶ月</p>
						<p>・　８月・・・燃料手当（1,866L分）</p>
						<p>・１２月・・・基本給×1.05の1.9ヶ月</p>
						<p>・１２月・・・基本給の1.2ヶ月</p>
						<p>・　２月・・・基本給×1.05の0.55ヶ月</p>
						<p>・特別賞与（業績による・前年実績・・・基本給×1.2ヶ月）</p>
					</td>
				</tr>
				<tr>
					<th>休日</th>
					<td>
						<p>日曜・祝日</p>
						<p>5/1、6/15、8/15・16、12/29〜1/5</p>
						<p>４週６休</p>
						<p>指定休日 ： ７日</p>
						<p>年間休日数 ： 100日</p>
					</td>
				</tr>
				<tr>
					<th>加入保険</th>
					<td>健康・厚生・雇用・労災</td>
				</tr>
				<tr>
					<th>応募書類</th>
					<td>履歴書・ 卒業（見込）証明書・成績証明書　</td>
				</tr>
				<tr>
					<th>求人数</th>
					<td>2018年4月採用 1〜3名　</td>
				</tr>
				<tr>
					<th>説明会</th>
					<td><span class="u-underline">＊詳細はこちらのリクナビをご覧ください</span></td>
				</tr>
			</table>
		</div>
	</div>
</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>