<!-- begin footer -->
<footer class="c-footer">
	<div class="l-container l-wrapper">
		<div class="c-footer__contact">
			<p><span class="u-breakline-sp u-mgR15">JA鹿追町</span>〒081-0341 北海道河東郡鹿追町新町4丁目51番地</p>
			<p><span class="u-breakline-sp u-mgR15">tel. 0156-66-2131</span>fax. 0156-66-3194</p>
			<div class="u-pc-only">
				<div class="l-btn l-btn--inline">
					<div class="c-btn"><a href="">アクセスマップ</a></div>
					<div class="c-btn"><a href="">お問い合わせ</a></div>
				</div>
			</div>
			<div class="u-sp-only">
				<div class="l-btn l-btn--column">
					<div class="c-btn"><a href="">アクセスマップ</a></div>
					<div class="c-btn"><a href="">お問い合わせ</a></div>
				</div>
			</div>
		</div>
		<div class="c-footer__nav">
			<nav>
				<ul>
					<li><a href="">個人情報保護方針の取り扱い</a></li>
					<li><a href="">貯金苦情受付</a></li>
					<li><a href="">共済苦情受付</a></li>
					<li><a href="">サイトマップ</a></li>
					<li><a href="">リンク</a></li>
				</ul>
			</nav>
		</div>
	</div>
</footer>
<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>