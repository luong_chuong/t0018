<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>
<div class="c-dev-title3">c-btn</div>
<div class="c-btn">
	<a href="">Button</a>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">l-btn</div>
<div class="c-dev-title3">l-btn--inline</div>
<div class="l-btn l-btn--inline">
	<div class="c-btn">
		<a href="">Button</a>
	</div>
		<div class="c-btn">
		<a href="">Button</a>
	</div>
		<div class="c-btn">
		<a href="">Button</a>
	</div>
</div>
