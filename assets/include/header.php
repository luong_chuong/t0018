<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<!-- begin header -->
<section class="c-slogan">
	<div class="l-container">
		<h1>平成29年度スローガン「農〜 魅せる〜」</h1>
	</div>
</section>

<header class="c-header">
	<div class="l-container">
		<div class="c-header__logo"><a href=""><img src="/assets/image/common/logo.png" alt="" width="202" height="31"></a></div>
		<!-- global navigator-->
		<div class="c-header__nav">
			<nav class="c-gnavi" id="gnavi">
				<ul>
					<li><a href="">ホーム</a></li>
					<li class="is-current"><a href="">JA鹿追町について</a></li>
					<li><a href="">鹿追町の農業</a></li>
					<li><a href="">青年部・女性部・熟年会</a></li>
					<li><a href="">組合員情報</a></li>
					<li><a href="">職場紹介</a></li>
					<li><a href="">農業求人</a></li>
					<li><a href="">新着情報</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>
<!-- /.end header -->
<!-- begin header for SP -->
<header class="c-headerSP">
	<div class="l-container">
		<div class="c-headerSP__logo"><a href=""><img src="/assets/image/common/logo.png" alt="" width="130"></a></div>
		<div class="c-headerSP__menu">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<nav class="c-naviSP">
		<ul>
			<li><a href="">ホーム</a></li>
			<li><a href="">JA鹿追町について</a></li>
			<li><a href="">鹿追町の農業</a></li>
			<li><a href="">青年部・女性部・熟年会</a></li>
			<li><a href="">組合員情報</a></li>
			<li><a href="">職場紹介</a></li>
			<li><a href="">農業求人</a></li>
			<li><a href="">新着情報</a></li>
		</ul>
	</nav>
</header>