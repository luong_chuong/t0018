<?php $id="recipe";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<!-- section 1 -->
<section class="p-recipe1">
	<div class="l-container">
		<!-- title -->
		<div class="c-titleSection">
			<h2>とっておきのレシピ</h2>
			<p>My favorite Recipe </p>
		</div>
		<!-- content -->
		<div class="p-recipe1__block">
			<div class="p-recipe1__headline">
				<p>鹿追産の食材を使ったお料理</p>
				<h3>カボカレーコロッケ</h3>
			</div>
			<div class="l-wrapper">
				<div class="c-imgbox">
					<div class="c-imgbox__image"><img src="assets/image/recipe/food.jpg" alt="" width="510"></div>
					<div class="c-imgbox__title"><h4>ワンポイント</h4></div>
					<div class="c-imgbox__text">
						<p>料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポイントアドバ
						イスを掲載。料理のコツやワンポイントアドバイスを掲載。料理のコツやワンポ
						イントアドバイスを掲載。料理のコツやワンポイントアドバイスを掲載。</p>
					</div>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>材料・調味料</th>
							<th>分量</th>
							<th>備考</th>
						</tr>
						<tr>
							<td>カボチャ</td>
							<td>1個</td>
							<td></td>
						</tr>
						<tr>
							<td>バター</td>
							<td>小さじ1</td>
							<td></td>
						</tr>
						<tr>
							<td>塩コショウ</td>
							<td>少々</td>
							<td></td>
						</tr>
						<tr>
							<td>カレーパウダー</td>
							<td>大さじ3</td>
							<td></td>
						</tr>
						<tr>
							<td>ピザ用チーズ</td>
							<td>適量</td>
							<td></td>
						</tr>
						<tr>
							<td>（衣）卵</td>
							<td>1個</td>
							<td></td>
						</tr>
						<tr>
							<td>水</td>
							<td>50cc</td>
							<td></td>
						</tr>
						<tr>
							<td>薄力粉</td>
							<td>大さじ4</td>
							<td></td>
						</tr>
						<tr>
							<td>パン粉</td>
							<td>適量</td>
							<td></td>
						</tr>
						<tr>
							<td>黒ゴマ</td>
							<td>適量</td>
							<td></td>
						</tr>
						<tr>
							<td>揚げ油</td>
							<td>適量</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="p-recipe1__block">
			<div class="c-titleBg"><h4>作り方</h4></div>
			<div class="p-recipe1__step">
				<ul class="l-wrapper">
					<li>
						<p class="u-step">step 1</p>
						<p>カボチャは適当な大きさに切って種を取り、皮をむく。</p>
					</li>
					<li>
						<p class="u-step">STEP 2</p>
						<p>鍋に（1）のカボチャとかぶるくらいの水、バターを入れ、火にかけて柔らかくなるまで煮る。</p>
					</li>
					<li>
						<p class="u-step">STEP 3</p>
						<p>串が通る程度に柔らかくなったら煮汁を捨て、再び火にかけて水気を飛ばす。</p>
					</li>
					<li>
						<p class="u-step">STEP 4</p>
						<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレーパウダーで調味しながら粒が残る程度に潰しておく。</p>
					</li>
					<li>
						<p class="u-step">STEP 5</p>
						<p>（4）を好みの大きさに取り、ピザ用チーズを包み込むように形作る。20分たったら火をとめ粗熱をとり保存袋に入れる。</p>
					</li>
					<li>
						<p class="u-step">STEP 6</p>
						<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを加えたパン粉を付ける。</p>
					</li>
					<li>
						<p class="u-step">STEP 7</p>
						<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレーパウダーで調味しながら粒が残る程度に潰しておく。</p>
					</li>
					<li>
						<p class="u-step">STEP 8</p>
						<p>（4）を好みの大きさに取り、ピザ用チーズを包み込むように形作る。20分たったら火をとめ粗熱をとり保存袋に入れる。</p>
					</li>
					<li>
						<p class="u-step">STEP 9</p>
						<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを加えたパン粉を付ける。</p>
					</li>
					<li>
						<p class="u-step">STEP 10</p>
						<p>（3）を人肌くらいまで冷ましたら、塩コショウ、カレーパウダーで調味しながら粒が残る程度に潰しておく。</p>
					</li>
					<li>
						<p class="u-step">STEP 11</p>
						<p>（4）を好みの大きさに取り、ピザ用チーズを包み込むように形作る。20分たったら火をとめ粗熱をとり保存袋に入れる。</p>
					</li>
					<li>
						<p class="u-step">STEP 12</p>
						<p>（5）に衣を付ける。卵と水は合わせて溶き、薄力粉を加えて混ぜる。（5）を卵液にくぐらせ、黒ゴマを加えたパン粉を付ける。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- section 2 -->
<section class="p-recipe2">
	<div class="l-container">
		<!-- title -->
		<div class="c-titleSection">
			<h2>おすすめレシピ</h2>
			<p>Archive</p>
		</div>
		<!-- content -->
		<!-- block -->
		<div class="p-recipe2__block">
			<div class="c-titleBg"><h4>和食</h4></div>
			<ul class="l-wrapper">
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<!-- block -->
		<div class="p-recipe2__block">
			<div class="c-titleBg"><h4>洋食・中華</h4></div>
			<ul class="l-wrapper">
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<!-- block -->
		<div class="p-recipe2__block">
			<div class="c-titleBg"><h4>デザート・ケーキ</h4></div>
			<ul class="l-wrapper">
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
				<li>
					<div class="c-card">
						<div class="c-card__image"><img src="assets/image/recipe/fakeimg.png" alt="" width="220" height="114"></div>
						<div class="c-card__text">
							<p>レシピ名レシピ名レシピ名レシピレシピ名レシピ名レシピ名レシピ</p>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</div>
</section>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>